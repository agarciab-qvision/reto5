package com.choucair.formacion.definition;

import com.choucair.formacion.steps.WordPressSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class WordPressDefinition {

	@Steps
	WordPressSteps wordPressSteps;
	
	@Given("^Acceso sitio WordPress$")
	public void acceso_sitio_WordPress()  {
		wordPressSteps.ingresar();
	}

	@When("^Diligenciamiento de informacion$")
	public void diligenciamiento_de_informacion() throws InterruptedException {
		
		wordPressSteps.diligenciarInformacion();
	}

	
	@When("^Darle direccion al sitio$")
	public void darle_direccion_al_sitio() throws InterruptedException  {
		
		wordPressSteps.buscarDireccion();;
	}

	@When("^Seleccionar plan$")
	public void seleccionar_plan() {
		wordPressSteps.seleccionarPlan();
	    
	}
	@Then("^Verificacion de creacion de sitio en WordPress$")
	public void verificacion_de_creacion_de_sitio_en_WordPress()  {

	}
}
