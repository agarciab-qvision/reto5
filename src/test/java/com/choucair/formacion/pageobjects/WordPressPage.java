package com.choucair.formacion.pageobjects;

import static org.junit.Assert.assertThat;

import static org.hamcrest.Matchers.containsString;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://es.wordpress.com/")

public class WordPressPage extends PageObject {

	@FindBy (id = "navbar-getstarted-link")
    public WebElementFacade lnkEmpezar;

	@FindBy (id="step-header")
    public WebElementFacade lblCabecera;
	
	//Datos formulario Uno
	
	@FindBy (id="siteTitle")
    public WebElementFacade txtNombreSitio;

	@FindBy (id="siteTopic")
    public WebElementFacade txtTopico;
	
	@FindBy (id="share")
    public WebElementFacade chkComparte;
	
	@FindBy (id="promote")
    public WebElementFacade chkPromociona;
	
	
	@FindBy(xpath = "//*[@id=\'search-component-11\']")
    public WebElementFacade txtBuscarNombreSitio;
	

	
	@FindBy(xpath="//*[@id=\'primary\']/span/span/div/div/div/div[1]/div[1]/div/div/div[2]/svg")
    public WebElementFacade btnLupa;
	
	
	
	
	@FindBy(xpath="//*[@id=\'primary\']/span/span/div/div/div/div[1]/div[2]/form/div[1]/fieldset[4]/div/ul/li[2]/a/span")
    public WebElementFacade btnNivel2;
	
	@FindBy(xpath="//*[@id=\"primary\"]/span/span/div/div/div/div[1]/div[2]/form/div[2]/button")
    public WebElementFacade btnGratis;
	
	@FindBy(xpath = "//*[@id=\'primary\']/span/span/div/div/div/div/div[1]/div/div[4]/div/div/button")
    public WebElementFacade btnComenzarGratis;
	
	
	
	public void VerificarIngresoSitio() {
		String lblVerificacion = "Empezar";
		String strMensaje = lnkEmpezar.getText();
		assertThat(strMensaje, containsString(lblVerificacion));
	}


	public void FormularioUno() {
		lnkEmpezar.click();
		String lblVerificacion = "Vamos a crear un sitio";
		String strMensaje = lblCabecera.getText();
		assertThat(strMensaje, containsString(lblVerificacion));
		
		
	}


	public void DiligenciarFormularioUno() throws InterruptedException {
		txtNombreSitio.click();
		//temporizador(2);
		txtNombreSitio.clear();
		txtNombreSitio.sendKeys("reto5agb");
	//	temporizador(2);
		txtTopico.click();
	//	temporizador(2);
		txtTopico.clear();
		txtTopico.sendKeys("Automatizacion");
	//	temporizador(2);
		chkComparte.click();
	//	temporizador(2);
		chkPromociona.click();
	//	temporizador(2);
		btnNivel2.click();
		btnGratis.click();
	}


	public void DiligenciarFormularioDos() throws InterruptedException {
	
		
		txtBuscarNombreSitio.click();
		
		txtBuscarNombreSitio.sendKeys("reto5agb");
		
		temporizador(10);
		
		
		btnLupa.click();
		
		
		
		

		
		
	}


	public void DiligenciarFormularioTres() {
		btnComenzarGratis.click();
		
	}
	
	public void ValidarFormularioDos() {
		lnkEmpezar.click();
		String lblVerificacion = "Vamos a crear un sitio";
		String strMensaje = lblCabecera.getText();
		assertThat(strMensaje, containsString(lblVerificacion));
		
		
	} 
	
	public void temporizador(int intervaloEspera) throws InterruptedException {
		Thread.sleep(intervaloEspera * 1000);
		
	}
}
