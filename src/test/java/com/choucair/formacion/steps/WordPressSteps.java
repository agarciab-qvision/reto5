package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.WordPressPage;

import net.thucydides.core.annotations.Step;

public class WordPressSteps {

	WordPressPage wordPressPage;

	@Step
	public void ingresar() {
		wordPressPage.open();
		wordPressPage.VerificarIngresoSitio();
		
	}

	@Step
	public void diligenciarInformacion() throws InterruptedException   {
		
		wordPressPage.FormularioUno();
		
		wordPressPage.DiligenciarFormularioUno();
	

		
		
		
	}

	public void buscarDireccion() throws InterruptedException {
		wordPressPage.DiligenciarFormularioDos();
		
	}



	public void seleccionarPlan() {
		wordPressPage.DiligenciarFormularioTres();
		
	}
}
