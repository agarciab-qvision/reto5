#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Creacion sitio web en WordPress
  I want to use this template for my feature file

  @CasoExitoso
  Scenario: Creacion sitio web en WordPress
    Given Acceso sitio WordPress
    When Diligenciamiento de informacion
    And Darle direccion al sitio
    And Seleccionar plan
    Then Verificacion de creacion de sitio en WordPress

